# mirrorConsole #

Debug webapps on mobile devices without connecting them to a computer (and more importantly, debug an iPhone without a Mac).

### Installation ###
* Clone this repo.
* run `npm install`.

### Run the servers ###
* Run `npm run HTTP`.
* A simple HTTP server will start, it will be used to serve the Javascript to the client - note the ip addresses suggested.
* Run `npm run WS`.
* The websocket server will be launched and start listening for the incoming console messages.

### Set up the client ###
* In the head of the HTML file you need to debug copy `<script src="http://<one-of-the-ips-suggested-by-the-HTTP-server>:9998/client.js"></script>`
* Serve it how you normally do through a web server and you start seeing flowing logs in the console where you fired the server

### Settings ###

#### Host ####
You can change the ip address and the port for the websocket server in the config.json file.

#### Colors ####
You can change the colors based on the console event (log, info, warn and error), by editing the file config.json.
You can find all the color options on the _Chalk_ readme page https://github.com/chalk/chalk.

### Troubleshooting ###
If you don't see the logs flowing in the console as expected, try changing the host ip address from the client.js file to a working one.
You can start trying with those suggested by the HTTP server.

```
// var hostIP = location.host.split(':')[0];
var hostIP = '192.168.1.1';
```

That's it.
If you don't see the colors on Windows, try http://cmder.net/.