const WebSocket = require('ws')
const chalk = require('chalk')
const config = require('./config.json')

const ws = new WebSocket.Server(config.websocket)
const colors = config.colors

console.log(chalk[colors['server']]('    > websocket ready'))
ws.on('connection', socket => {
    console.log(chalk[colors['server']]('    > new connection'))

    socket.on('message', function(req) {
        const data = JSON.parse(req)
        console.log(chalk[colors[data.type]](data.data))
    })

    socket.on('close', error => {
        console.log(chalk[colors['server']](`    > connection closed (${error})`))
        socket.close()
    })
})
