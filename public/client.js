(function hijack(){
			var hostIP = location.host.split(':')[0];

			var ws = new WebSocket('ws://' + hostIP + ':9999');
			ws.onopen = function() {
				console.info('    > WS connected');
			}
			var localLog = console.log;
			console.log = function () {
				var data = Array.prototype.slice.call(arguments, hijack.length).join(' ');
				ws.send(JSON.stringify({data:data, type: 'log'}));
				localLog.apply(console, arguments);
    	}
			var localInfo = console.info;
			console.info = function () {
				var data = Array.prototype.slice.call(arguments, hijack.length).join(' ');
				ws.send(JSON.stringify({data:data, type: 'info'}));
				localInfo.apply(console, arguments);
    	}
			var localWarn = console.warn;
			console.warn = function () {
				var data = Array.prototype.slice.call(arguments, hijack.length).join(' ');
				ws.send(JSON.stringify({data:data, type: 'warn'}));
				localWarn.apply(console, arguments);
    	}
			var localError = console.error;
			console.error = function () {
				var data = Array.prototype.slice.call(arguments, hijack.length).join(' ');
				ws.send(JSON.stringify({data:data, type: 'error'}));
				localError.apply(console, arguments);
    	}
})();
